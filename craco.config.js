/* craco.config.js */
const CracoLessPlugin = require('craco-less');
const path = require('path');

module.exports = {
    // ...
    plugins: [
        {
            plugin: CracoLessPlugin,
            options: {
                lessLoaderOptions: {
                    lessOptions: {
                        modifyVars: { '@primary-color': '#f5222d' },
                        javascriptEnabled: true,
                    },
                },
            },
        },
    ]
};
