import {Table, Badge, Menu, Dropdown, Space, Button} from 'antd';
import React from "react";
import { DownOutlined } from '@ant-design/icons';

const menu = (
    <Menu>
        <Menu.Item>Action 1</Menu.Item>
        <Menu.Item>Action 2</Menu.Item>
    </Menu>
);

function TaskTable() {

    const columns = [
        { title: '任务名称', dataIndex: 'date', key: 'date' },
        { title: '任务类型', dataIndex: 'taskType', key: 'taskType' },
        { title: '工作量', dataIndex: 'workload', key: 'workload' },
        { title: '执行人', dataIndex: 'userName', key: 'userName' },
        {
            title: '状态',
            key: 'taskState',
            render: () => (
                <span>
            <Badge status="success" />
            完成
          </span>
            ),
        },
        {
            title: '操作',
            dataIndex: 'operation',
            key: 'operation',
            render: () => (
                <Space size="middle">
                    <a>Pause</a>
                    <a>Stop</a>
                    <Dropdown overlay={menu}>
                        <a>
                            More <DownOutlined />
                        </a>
                    </Dropdown>
                </Space>
            ),
        },
    ];

    const data = [];
    for (let i = 0; i < 3; ++i) {
        data.push({
            key: i,
            name: 'Screem',
            platform: 'iOS',
            version: '10.3.4.5654',
            upgradeNum: 500,
            creator: 'Jack',
            createdAt: '2014-12-24 23:12:00',
        });
    }

    return (
        <Table
            className="components-table-demo-nested"
            columns={columns}
            dataSource={data}
        />
    );
}
export default TaskTable;
