import React from "react";
import {TaskTable} from "./index";
import {useAddTask} from "./hooks";
import {NormalButtonLayout,NormalOptionsButton} from "../../../core";

const Task=()=>{
    let {open,ModalForm}=useAddTask();
    return <div>

        <NormalOptionsButton add={{fn:open}} />
        <ModalForm/>
        <TaskTable/>
    </div>
};
export default Task;
