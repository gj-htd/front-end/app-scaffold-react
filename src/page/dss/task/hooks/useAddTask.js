import React, {useEffect, useState} from "react";
import {Button, Col, Drawer, Form, Input, Tooltip, message, Space, Modal, Checkbox, Row} from "antd";
import {UserOutlined,QuestionCircleOutlined,PlusOutlined} from '@ant-design/icons';
import {useRequest} from "ahooks";
import api from "../api/api";
import TaskTypeSelect from "../Component/TaskTypeSelect";
const { TextArea } = Input;

const layout = {
    labelCol: {
        span: 6,
    },
    wrapperCol: {
        span: 16,
    },
};
const tailLayout = {
    wrapperCol: {
        offset: 8,
        span: 16,
    },
};

const useAddTask=(props)=>{
    const [form] = Form.useForm();
    const [visible,setVisible] =useState(false);

    console.log(props);
    /**
     * 显示/
     * @param visible
     */
    function show(visible) {
        setVisible(visible);
    }
    const { data, loading,error,cancel,refresh,run } = useRequest((value) => api.save(value),{manual: true});

    /**
     * 表单提交
     * @param value
     */
    const onFinish=(value)=>{
        run(value).then(function () {
            show(false);
            if(props.onLoad && typeof(props.onLoad)==="function"){
                props.onLoad();
            }
        });
        console.log(value);
    };

    /**
     * 提交失败响应
     * @param err
     */
    const onFinishFailed=(err)=>{
        console.log(err);
    }
    /**
     * Form表单
     * @returns {*}
     * @constructor
     */
    const FormApp=()=>{
        return <div>
            <Form    {...layout} onFinish={onFinish} onFinishFailed={onFinishFailed} form={form} scrollToFirstError={true} initialValues={{
                remember: true,
            }}>
                <Form.Item
                    name="id"
                    hidden={true}
                />
                <Form.Item
                    name="userStoryId"
                    hidden={true}
                />
                <Form.Item
                    name="taskName"
                    label="任务名称"
                    rules={[{required: true, message: '请输入任务名称'}]}
                >
                    <Input placeholder="请输入任务名称"/>
                </Form.Item>

                <Form.Item
                    name="taskType"
                    label="任务类型"
                    rules={[{required: true, message: '请选择任务类型'}]}
                >
                    <TaskTypeSelect/>
                </Form.Item>

                <Form.Item
                    name="basePath"
                    label={
                        <span>基本路径
                        <Tooltip title="接口基本路径，为空是根路径">
                            <QuestionCircleOutlined />
                        </Tooltip>
                        </span>
                    }
                    rules={[{required: true, message: '请输入基本路径'}]}
                >
                    <Input placeholder="请输入作者姓名" prefix={<UserOutlined className="site-form-item-icon" />}/>
                </Form.Item>

                <Form.Item
                    name="description"
                    label="描述"
                >
                    <TextArea  placeholder="可以选择添加描述" prefix={<UserOutlined className="site-form-item-icon" />}/>
                </Form.Item>
            </Form>
        </div>
    }
    /**
     * Modal的表单
     * @param title
     * @param id
     * @returns {*}
     * @constructor
     */
    const DrawerFormApp=({title,id})=>{
        return <>
            <Button  onClick={()=>{
                show(true);
            }}>
                <PlusOutlined /> {title?title:"新增"}
            </Button>
            <Drawer
                title="新增一个项目"
                width={720}
                onClose={()=>{
                    show(false)
                }}
                visible={visible}
                bodyStyle={{paddingBottom: 80}}
                footer={
                    <div
                        style={{
                            textAlign: 'right',
                        }}
                    >
                        <Button
                            onClick={()=>{
                                show(false)
                            }}
                            style={{marginRight: 8}}
                        >
                            取消
                        </Button>
                        <Button onClick={()=>{
                            form.submit();
                        }} type="primary">
                            提交
                        </Button>
                    </div>
                }
            ><FormApp id={id}/></Drawer>
        </>
    }

    /**
     * 增加项目
     * @returns {*}
     * @constructor
     */
    const ModalForm=({title,id})=>{
        return (<div>
            <Modal
                title="新建项目"
                visible={visible}
                onOk={()=>{
                    form.submit();
                }}
                onCancel={()=>{
                    show(false);
                }}
                footer={
                    <div
                        style={{
                            textAlign: 'right',
                        }}
                    >
                        <Button
                            onClick={()=>{
                                show(false)
                            }}
                            style={{marginRight: 8}}
                        >
                            取消
                        </Button>
                        <Button onClick={()=>{
                            form.submit();
                        }} type="primary">
                            提交
                        </Button>
                    </div>
                }
            >
                <FormApp id={id}/>
            </Modal>
        </div> )
    }

    /**
     * 手动打开窗口
     * @param options {initialValue:"默认值"}
     */
    const open= (options)=>{
        let initialValue=options?options.initialValue:null;
        form.resetFields();
        if(initialValue){
            form.setFieldsValue(initialValue);
        }
        show(true);
    }

    return {ModalForm,open}
}

export  {useAddTask};
