import React from "react";
import {Select} from "antd";
const {Option}=Select;

const TaskTypeSelect=({handleChange})=>{
    return (
        <Select defaultValue="选择任务类型" style={{ width: "100%" }} onChange={handleChange}>
            <Option value={2}>后端开发</Option>
            <Option value={3} disabled>
                前端开发
            </Option>
            <Option value={0} disabled>设计</Option>
            <Option value={1} disabled>产品</Option>

            <Option value={4} disabled>测试</Option>
            <Option value={5} disabled>系统保障部</Option>
            <Option value={6} disabled>其他</Option>
        </Select>)
};

export default TaskTypeSelect;
