import {Select} from "antd";
import React from "react";
const {Option}=Select;
const DeptSelect=({handleChange})=>{
    return (
        <Select defaultValue="请选择部门" style={{ width: "100%" }} onChange={handleChange}>
            <Option value={0}>后台产品部</Option>
            <Option value={1}>大数据部</Option>
            <Option value={2}>研发管理部</Option>
            <Option value={3} disabled>
                产业平台部
            </Option>
            <Option value={4}>中台产品部</Option>
            <Option value={5}>系统保障部</Option>
            <Option value={6}>其他</Option>
        </Select>)
};

export default DeptSelect;
