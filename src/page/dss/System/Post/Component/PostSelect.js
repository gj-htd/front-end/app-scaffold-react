import {Select} from "antd";
import React from "react";
const {Option}=Select;
const PostSelect=(props)=>{
    return (
        <Select defaultValue="请选择岗位" style={{ width: "100%" }} {...props}>
        <Option value={0}>后端开发</Option>
        <Option value={1}>前端开发</Option>
        <Option value={2}>UI设计</Option>
        <Option value={3} disabled>
            设计
        </Option>
        <Option value={4}>项目负责人</Option>
        <Option value={5}>技术负责人</Option>
        <Option value={6}>其他</Option>
    </Select>)
};

export default PostSelect;
