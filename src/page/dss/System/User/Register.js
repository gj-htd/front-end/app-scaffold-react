import {Button, Checkbox, Col, Form, Input, Row, Space} from "antd";
import React from "react";
import {LockOutlined, UserOutlined} from '@ant-design/icons';
import coreSettingConfig from "../../../../core/coreSettingConfig";
import {useHistory} from "react-router";

import './Register.less'
import {PostSelect} from "../Post";
import {DeptSelect} from "../Dept";
const Register=()=>{

    let history = useHistory();
    /**
     * 表单提交
     * @param value
     */
    const onFinish=(value)=>{
        console.log(value);
        history.push("/login");
    };

    /**
     * 提交失败响应
     * @param err
     */
    const onFinishFailed=(err)=>{
        console.log(err);
    }

    return (
        <header className="App-header">
            <Space direction="vertical">
                <div align="center" className="login-img">
                    <img alt={'logo'} src={coreSettingConfig.Logo}  />
                </div>
                <Form
                    name="normal_login"
                    className="login-form"
                    initialValues={{
                        remember: false,
                    }}
                    onFinish={onFinish}
                >
                    <Form.Item
                        name="account"
                        rules={[
                            {
                                required: true,
                                message: '请输入您的登录账号!',
                            },
                        ]}
                    >
                        <Input prefix={<UserOutlined className="site-form-item-icon"/>} placeholder="账号"/>
                    </Form.Item>

                    <Form.Item
                        name="邮箱"
                    >
                        <Input type={"email"} placeholder="邮箱"/>
                    </Form.Item>

                    <Form.Item
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: '请输入您的密码!',
                            },
                        ]}
                    >
                        <Input.Password
                            prefix={<LockOutlined className="site-form-item-icon"/>}
                            type="password"
                            placeholder="密码"
                        />
                    </Form.Item>
                    <Form.Item
                        name="password2"
                        rules={[
                            {
                                required: true,
                                message: '请确认您的密码!',
                            },
                        ]}
                    >
                        <Input.Password
                            prefix={<LockOutlined className="site-form-item-icon"/>}
                            placeholder="确认密码"
                        />
                    </Form.Item>


                    <Form.Item
                        name="dept"
                    >
                        <DeptSelect>部门</DeptSelect>
                    </Form.Item>

                    <Form.Item
                        name="post"
                        rules={[
                            {
                                required: true,
                                message: '岗位!',
                            },
                        ]}
                    >
                        <PostSelect/>
                    </Form.Item>

                    <Form.Item
                        name="isOutSourcingUserType"
                    >
                        <Checkbox>外包人员</Checkbox>
                      </Form.Item>

                    <Form.Item>
                        <Space align="end">
                            <Button type="primary" htmlType="submit" >
                                注册
                            </Button>
                            <Button  onClick={()=>{
                                history.push("/");
                            }}>
                                返回
                            </Button>
                        </Space>

                    </Form.Item>
                </Form>
            </Space>
        </header>
    )
};
export default Register;
