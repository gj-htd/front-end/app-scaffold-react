
import { Tabs, Select, Space } from 'antd';
import React, {useState} from "react";
import ProjectCard from "./ProjectCard";
import {Route, useHistory} from "react-router";
import {UserTable} from "../System";

const { TabPane } = Tabs;

const Project=(props)=>{
    console.log(props)
    return <Tabs >
        <TabPane tab="项目" key="1">
                <ProjectCard/>
        </TabPane>
        <TabPane tab="部门成员" key="2">
            <UserTable/>
        </TabPane>
        <TabPane tab="设置" key="3">
            Content of Tab 3
        </TabPane>
    </Tabs>
}

export default Project;
