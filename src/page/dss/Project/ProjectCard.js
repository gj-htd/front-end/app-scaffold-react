import {Avatar, Badge, Button, Card, Col, Icon, Popconfirm, Progress, Row, Space, Spin} from "antd";
import React, {useEffect, useState} from "react";
import {useAddProject} from "./hooks/useAddProject";
import {Route, useHistory} from "react-router";
import {useRequest} from "ahooks";
import api from "./api/api";
import { EditOutlined, EllipsisOutlined, SettingOutlined,PlusOutlined,DeleteOutlined,QuestionCircleOutlined } from '@ant-design/icons';
import ProjectStateUtil from "./utils/ProjectStateUtil";
import {NormalOptionsButton} from "../../../core";

const { Meta,Grid } = Card;
const gridStyle = {
    width: '25%',
    textAlign: 'center',
};

/**
 * 卡片明细
 * @param data
 * @returns {null|*}
 * @constructor
 */
let CardItem=({data,handleEdit,handleDel})=>{
    let history=useHistory();
    if(data && data.success && data.data){
        return data.data.map(function (item) {
            let stateColor = ProjectStateUtil.getCole(item.state);
            return <Col span={8} key={item.id}>
                <Badge.Ribbon text={ProjectStateUtil.getStateTitle(item.state)}color={stateColor}>
                    <Card  title={<Progress percent={item.progress} />} bordered={true} hoverable={true} actions={[
                        <SettingOutlined key="setting" />,
                        <Popconfirm title="确定要删除？" icon={<QuestionCircleOutlined style={{ color: 'red' }} />} onConfirm={()=>{
                            handleDel(item.id);
                        }}>
                            <DeleteOutlined/>
                        </Popconfirm>,

,                            <EditOutlined key="edit" onClick={()=>{
                            handleEdit({initialValue:item})
                        }}/>,
                        <EllipsisOutlined key="ellipsis" />,
                    ]}>
                        <Meta
                            avatar={<Avatar size={50} style={{
                                backgroundColor: stateColor,
                                verticalAlign: 'middle',
                            }}>{item.title}</Avatar>}
                            description={item.description}
                            title={item.title}
                            onClick={()=>{
                                history.push("/project-info",{id:item.id});
                            }}
                        />
                    </Card>
                </Badge.Ribbon>
            </Col>
        })
    }else{
        return null;
    }
}


/***
 * 项目卡片组件
 * @returns {*}
 * @constructor
 */
const ProjectCard=()=>{
    let [params,setParams]=useState({});
    const { data,run,refresh } = useRequest(api.listPage,{manual:true});

    let delResult=useRequest(api.del,{manual:true});

    const onLoad=()=>{
        run(params).then(function (data) {
            console.log(data);
        });
    };

    /**
     * 删除
     * @param ids
     */
    const del=(ids)=>{
        delResult.run(ids).then(function () {
            refresh();
        })
    }

    useEffect(function () {
        onLoad(params);
    },[])

    let {open,ModalForm}= useAddProject({onLoad:onLoad});

    let options={
        add:{
            fn:open,
            title:"新增"
        },
        del:{}
    }
    return (
        <div>
            <NormalOptionsButton {...options}/>

            <ModalForm onLoad={onLoad}/>
            <Row gutter={[16,16]}>
                <CardItem data={data} handleEdit={open} handleDel={del}/>
            </Row>
        </div>

    )
}
export default ProjectCard;
