import { Table, Tag, Space } from 'antd';
import React from "react";

const columns = [
    {
        title: '接口名称',
        dataIndex: 'title',
        key: 'title',
        render: text => <a>{text}</a>,
    },
    {
        title: '接口路径',
        dataIndex: 'path',
        key: 'path',
    },
    {
        title: '接口分类',
        dataIndex: 'groupName',
        key: 'groupName',
    },
    {
        title: '状态',
        dataIndex: 'state',
        key: 'address',
    },
    {
        title: '岗位',
        key: 'tags',
        dataIndex: 'tags',
        render: tags => (
            <>
                {tags.map(tag => {
                    let color = tag.length > 5 ? 'geekblue' : 'green';
                    if (tag === 'loser') {
                        color = 'volcano';
                    }
                    return (
                        <Tag color={color} key={tag}>
                            {tag.toUpperCase()}
                        </Tag>
                    );
                })}
            </>
        ),
    },
    {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
            <Space size="middle">
                <a>Invite {record.name}</a>
                <a>Delete</a>
            </Space>
        ),
    },
];

const data = [
    {
        key: '1',
        title: '用户操作接口',
        path: '/user',
        groupName: '用户接口',
        state: '已完成',
        address: 'New York No. 1 Lake Park',
        tags: ['后端开发', '技术经理'],
    },
    {
        key: '2',
        name: 'Jim Green',
        age: 42,
        address: 'London No. 1 Lake Park',
        tags: ['loser'],
    },
    {
        key: '3',
        name: 'Joe Black',
        age: 32,
        address: 'Sidney No. 1 Lake Park',
        tags: ['cool', 'teacher'],
    },
];

const InterfaceTable=()=>{
    return <Table columns={columns} dataSource={data} />
}
export default InterfaceTable;

