import React from "react";
import {Divider, Layout, Space} from "antd";
import ApiTree from "./InterfaceTree";
import InterfaceTable from "./InterfaceTable";

const { Header, Content, Footer, Sider } = Layout;

const Interface=()=>{
    return <div >
        <Layout>
            <Sider theme={"light"} width={"25%"}>
                <ApiTree/>
            </Sider>
            <Content className="site-layout-background">
                <InterfaceTable/>
            </Content>
        </Layout>
    </div>
};

export default Interface;
