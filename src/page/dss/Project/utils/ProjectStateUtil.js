/**
 * 项目状态工具
 * @type {{STATE_ING: number, STATE_READY: number, STATE_DOWN: number, STATE_NO_START: number}}
 */
let ProjectStateUtil={
    /**
     * 未开始
     */
    STATE_NO_START:0,
    /**
     * 准备中
     */
    STATE_READY:1,
    /**
     * 进行中
     */
    STATE_ING:1,
    /**
     * 完成
     */
    STATE_DOWN:1,

    /**
     * 获取状态标题
     * @param state
     * @returns {string}
     */
    getStateTitle:(state)=>{
        switch (state) {
            case ProjectStateUtil.STATE_NO_START:
                return "未开始"
            case ProjectStateUtil.STATE_READY:
                return "准备中"
            case ProjectStateUtil.STATE_ING:
                return "进行中"
            case ProjectStateUtil.STATE_DOWN:
                return "已完成"
            default :
                return "未开始"
        }
    },
    /**
     * 获取状态颜色
     * @param state
     * @returns {string}
     */
    getCole:(state)=>{
        switch (state) {
            case ProjectStateUtil.STATE_NO_START:
                return "#d9d9d9"
            case ProjectStateUtil.STATE_READY:
                return "#fadb14"
            case ProjectStateUtil.STATE_ING:
                return "#2db7f5"
            case ProjectStateUtil.STATE_DOWN:
                return "#52c41a"
            default :
                return "#d9d9d9"
    }
    }
}

export default ProjectStateUtil;
