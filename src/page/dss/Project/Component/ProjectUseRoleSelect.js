import React, {useEffect, useState} from "react";
import {Divider, Input, message, Select} from "antd";
import {useRequest} from "ahooks";
import { PlusOutlined } from '@ant-design/icons';
import {projectUseRoleApi} from "../api";
const { Option } = Select;
/**
 * 增加
 * @param menu
 * @param handleChange
 * @param name
 * @param addItem
 * @returns {*}
 * @constructor
 */
const AddTags=({menu,handleChange,name,addItem,delItem})=>{
    return <div>
        {menu}
        <Divider style={{ margin: '4px 0' }} />
        <div style={{ display: 'flex', flexWrap: 'nowrap', padding: 8 }}>
            <span><Input style={{ flex: 'auto' }} value={name} onChange={handleChange} /></span>
            <a
                style={{ flex: 'none', padding: '8px', display: 'block', cursor: 'pointer' }}
                onClick={addItem}
            >
                <PlusOutlined /> 增加
            </a>

            <a
                style={{ flex: 'none', padding: '8px', display: 'block', cursor: 'pointer' }}
                onClick={delItem}
            >
                <PlusOutlined /> 删除
            </a>
        </div>
    </div>
}

/**
 * 项目使用角色选择组件
 * @param projectId
 * @returns {*}
 * @constructor
 */
const ProjectUseRoleSelect=({projectId,onChange})=>{

    let {loading,refresh,run}=useRequest(projectUseRoleApi.listPage,{manual:true});
    let addResult=useRequest(projectUseRoleApi.add,{manual:true});
    let delResult = useRequest(projectUseRoleApi.del,{manual:true});

    //let selected=[];
    let [name,setName]=useState("");
    let [items,setItems]=useState([]);
    let [selected,setSelected]=useState([]);

    let [param,setParam]=useState({projectId:projectId});


    let localRefresh=()=>{
        refresh().then(function (result) {
            console.log("--->zheli~~")
            console.log(result)
            if(result && result.data){
                setItems(result.data);
            }

        });
    }
    /**
     * 增加一项
     */
    const addItem=()=>{
        addResult.run({name:name,projectId:projectId}).then(function (result) {
            setName("");
            localRefresh();

        });
    };
    /***
     * 删除选中项
     */
    const delItem=()=>{
        if(selected.length>0){
            console.log(selected.toString());
            delResult.run(selected.toString()).then(function () {
                localRefresh();
                setSelected([]);
            });

        }else{
            message.error("选择后进行删除");
        }

    };
    let onSelect=(value,options)=>{
        console.log("-------------->")

        setSelected(value);
        if(onChange && typeof (onChange)==="function"){
            if(selected.length>0){
                console.log(selected)
                onChange(selected.toString());
            }else{
                console.log(value)
                onChange(value.toString())
            }

        }
        //selected=value;
    }

    let onNameChange = event => {
        setName(event.target.value);
    };


    let props={
        name:name,
        handleChange:onNameChange,
        addItem:addItem,
        delItem:delItem
    }
    useEffect(function () {
        run(param).then(function (result) {
            console.log("初始化")
            console.log(result)
            if(result && result.data){
                setItems(result.data);
            }
        });
    },[projectId]);

    return <Select mode="tags" style={{ width: '100%' }} onSelect={onSelect} loading={loading} allowClear tokenSeparators={[',']} dropdownRender={menu => (
        <AddTags menu={menu} {...props}/>
    )}>
        {items && items.length>0?items.map(item => (
            <Option key={item.id}>{item.name}</Option>
        )):null}
    </Select>
};

export default ProjectUseRoleSelect;
