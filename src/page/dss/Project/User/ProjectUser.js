import React from "react";
import {Tabs,Avatar,Tooltip} from "antd";
import { UserOutlined, AntDesignOutlined } from '@ant-design/icons';
import {ProjectUserTable} from "./";

/**
 * 项目成员
 * @constructor
 */
const ProjectUser=()=>{
    return <div>
        <Avatar.Group maxCount={10} maxStyle={{ color: '#f56a00', backgroundColor: '#fde3cf' }}>
            <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
            <Avatar style={{ backgroundColor: '#f56a00' }}>K</Avatar>
            <Tooltip title="Ant User" placement="top">
                <Avatar style={{ backgroundColor: '#87d068' }} icon={<UserOutlined />} />
            </Tooltip>
            <Avatar style={{ backgroundColor: '#1890ff' }} icon={<AntDesignOutlined />} />
        </Avatar.Group>
        <ProjectUserTable/>
    </div>
};



export default ProjectUser;
