import React, {useEffect, useState} from "react";
import {Button, Col, Drawer, Form, Input, Tooltip, message, Space, Modal, Checkbox, Row} from "antd";
import {UserOutlined,QuestionCircleOutlined,PlusOutlined} from '@ant-design/icons';
import {useRequest} from "ahooks";
import api from "../api/api";
import UserStoryRoleSelect from "../../Component/ProjectUseRoleSelect";

const { TextArea } = Input;

const layout = {
    labelCol: {
        span: 6,
    },
    wrapperCol: {
        span: 16,
    },
};
const tailLayout = {
    wrapperCol: {
        offset: 8,
        span: 16,
    },
};

const useAddUserStory=(props)=>{
    const [form] = Form.useForm();
    const [visible,setVisible] =useState(false);
    let projectId=props.projectId;
    /**
     * 显示/
     * @param visible
     */
    function show(visible) {
        setVisible(visible);
    }
    const { data, loading,error,cancel,refresh,run } = useRequest(api.add,{manual: true});

    /**
     * 表单提交
     * @param value
     */
    const onFinish=(value)=>{
        console.log(value)
        if(value && value.userRoleIds){
            value.userRoleIds=value.userRoleIds.toString();
        }
        run(value).then(function () {
            show(false);
            if(props.onLoad && typeof(props.onLoad)==="function"){
                props.onLoad();
            }
        });
    };

    /**
     * 提交失败响应
     * @param err
     */
    const onFinishFailed=(err)=>{
        console.log(err);
    }
    /**
     * Form表单
     * @returns {*}
     * @constructor
     */
    const FormApp=({id})=>{
        console.log("当前ID:"+id)
        return <div>
            <Form  layout="vertical" onFinish={onFinish} onFinishFailed={onFinishFailed} form={form} scrollToFirstError={true} initialValues={{
                remember: true,
            }}>
                <Form.Item
                    name="projectId"
                    rules={[{required: true, message: '未获取到项目ID'}]}
                    hidden={true}
                    initialValue={projectId}
                />
                <Form.Item
                    name="id"
                    hidden={true}
                />

                <Form.Item
                    name="userRoleIds"
                    label="用户角色"
                    rules={[{required: true, message: '请输入用户角色'}]}
                >
                    <UserStoryRoleSelect projectId={projectId}/>
                </Form.Item>

                <Form.Item
                    name="target"
                    label="目标"
                    rules={[{required: true, message: '请输入目标'}]}
                >
                    <Input placeholder="为了达成什么目标"/>
                </Form.Item>

                <Form.Item
                    name="price"
                    label="价值"
                    rules={[{required: true, message: '请输入中文价值'}]}
                >
                    <Input placeholder="这个功能可以带来什么价值?"/>
                </Form.Item>


                <Form.Item
                    name="description"
                    label="用户故事描述"
                >
                    <TextArea  placeholder="可以选择添加描述" prefix={<UserOutlined className="site-form-item-icon" />}/>
                </Form.Item>
            </Form>
        </div>
    }
    /**
     * Modal的表单
     * @param title
     * @param id
     * @returns {*}
     * @constructor
     */
    const DrawerFormApp=({title,id})=>{
        return <Drawer
                title="新增用户故事"
                width={720}
                onClose={()=>{
                    show(false)
                }}
                visible={visible}
                bodyStyle={{paddingBottom: 80}}
                footer={
                    <div
                        style={{
                            textAlign: 'right',
                        }}
                    >
                        <Button
                            onClick={()=>{
                                show(false)
                            }}
                            style={{marginRight: 8}}
                        >
                            取消
                        </Button>
                        <Button onClick={()=>{
                            form.submit();
                        }} type="primary">
                            提交
                        </Button>
                    </div>
                }
            ><FormApp id={id}/></Drawer>

    }

    /**
     * 增加项目
     * @returns {*}
     * @constructor
     */
    const ModalForm=({title,id})=>{
        return (<div>
            <Modal
                title="新建项目"
                visible={visible}
                onOk={()=>{
                    form.submit();
                }}
                onCancel={()=>{
                    show(false);
                }}
                footer={
                    <div
                        style={{
                            textAlign: 'right',
                        }}
                    >
                        <Button
                            onClick={()=>{
                                show(false)
                            }}
                            style={{marginRight: 8}}
                        >
                            取消
                        </Button>
                        <Button onClick={()=>{
                            form.submit();
                        }} type="primary">
                            提交
                        </Button>
                    </div>
                }
            >
                <FormApp id={id}/>
            </Modal>
        </div> )
    }

    /**
     * 增加项目的按钮条
     * @param props
     * @returns {*}
     * @constructor
     */
/*    const AddUserStoryButton =(props)=>{
        return (<Space style={{ marginBottom: 16 }} size={"small"}>
            {
               props.model===1?<DrawerFormApp {...props} key={"formModalAppKey"} id={props.moduleId}/>: <ModalForm title={"新增"} key={"formModalAppKey"} id={props.moduleId} onLoad={props.onLoad}/>
            }
                </Space>)
    }*/
    /**
     * 增加按钮条
     * @param props
     * @returns {*}
     * @constructor
     */
    const AddUserStoryButton =(props)=>{
        return (<Row justify="end">
            <Space style={{ marginBottom: 16 }} size={"small"}>
                <Col>
                    <Button  onClick={()=>{
                        open();
                    }}>
                        <PlusOutlined /> 新增
                    </Button>
                </Col>
            </Space>
        </Row>)
    }
    /**
     * 手动打开窗口
     * @param options {initialValue:"默认值"}
     */
    const open= (options)=>{
        let initialValue=options?options.initialValue:null;
        if(initialValue){
            form.resetFields();
            form.setFieldsValue(initialValue);
        }
        show(true);
    }
    return {AddUserStoryButton,ModalForm,DrawerFormApp,open}
}

export  {useAddUserStory};
