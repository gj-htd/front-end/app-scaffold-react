import {Table, Badge, Menu, Dropdown, Space, Button, Tag, Row, Col} from 'antd';
import React, {useEffect, useState} from "react";
import {TaskTable,Task} from "../../task";
import PropTypes from 'prop-types';
import {useRequest} from "ahooks";
import api from "./api/api";
import {useAddUserStory} from "./hooks/useAddUserStory";
import {Util} from "../../common";
import { EditOutlined, EllipsisOutlined, SettingOutlined,PlusOutlined,DeleteOutlined,QuestionCircleOutlined } from '@ant-design/icons';
import {NormalOptionsButton} from "../../../../core";


const menu = (
    <Menu>
        <Menu.Item>Action 1</Menu.Item>
        <Menu.Item>Action 2</Menu.Item>
    </Menu>
);

function UserStory({projectId}) {
    const expandedRowRender = () => {
        return <TaskTable/>;
    };


    const { data, loading,error,cancel,refresh,run} = useRequest(api.listPage,{manual:true});

    let [params,setParams]=useState({projectId:projectId});

    let onLoad=()=>{
        run(params);
    };

    useEffect(function () {
        onLoad();
    },[projectId])


    let {ModalForm,DrawerFormApp,open}=useAddUserStory({projectId:projectId,onLoad:onLoad});

    const columns = [
        { title: '用户角色', dataIndex: 'userRole', key: 'userRole', render:(value,record)=>{
            if(value && value.length>0){
                 return <div>{value.map((item,i)=>{
                     let color = Util.getRandomColor();
                     return <Tag color={color} key={i}>{item.name}</Tag>
                 })}</div>
            }
            return ""
            }},
        { title: '目标', dataIndex: 'target', key: 'target' },
        { title: '价值', dataIndex: 'price', key: 'price' },
        { title: '工作量', dataIndex: 'workload', key: 'workload' },
        { title: '状态', dataIndex: 'state', key: 'state' },
        { title: '负责人', dataIndex: 'dutyUserName', key: 'dutyUserName' },
        { title: '操作', key: 'operation', render: () => {

            return <div>
                <Button type="link">
                    拆解任务
                </Button>
            </div>
            } },
    ];

/*    for (let i = 0; i < 3; ++i) {
        data.push({
            key: i,
            name: 'Screem',
            platform: 'iOS',
            version: '10.3.4.5654',
            upgradeNum: 500,
            creator: 'Jack',
            createdAt: '2014-12-24 23:12:00',
        });
    }*/

let buttonProps={
    add:{
        fn:open,
        title:<><PlusOutlined />新增</>,
    }
}
    return (
        <div>
            <NormalOptionsButton {...buttonProps}/>
            <DrawerFormApp/>
            <Table
                className="components-table-demo-nested"
                columns={columns}
                expandable={{ expandedRowRender }}
                dataSource={data&&data.data?data.data:[]}
                rowKey="id"

            />
        </div>
    );
}

UserStory.propTypes={
    projectId:PropTypes.string.isRequired

}
export default UserStory;
