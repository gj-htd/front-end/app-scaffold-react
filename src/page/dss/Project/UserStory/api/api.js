import {BaseApi} from "../../../index";
let PATH="/user-story";
class CurrentApi extends BaseApi{
    constructor(){
        //调用父类的constructor(x,y)
        super(PATH);
    }
    userStoryRoleList=(params)=>{
        this.request.get(this.path+"/role",{data:params});
    }
};
let api = new CurrentApi(PATH);
export default api;

