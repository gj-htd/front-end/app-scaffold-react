import React from "react";
import {Layout, Tabs} from "antd";
import {UserStory} from "../UserStory";
import Interface from "../Interface/Interface";
import ProjectUser from "../User/ProjectUser";
import {TMetaDataTable} from "../../MetaData";
const { TabPane } = Tabs;

const ProjectInfo=(props)=>{
    let {location}=props;
    let id;
    if(location && location.state){
        id=location.state.id;
    }

    return <div >
        <Tabs >
            <TabPane tab="用户故事" key="1">
                <UserStory projectId={id}/>
            </TabPane>
            <TabPane tab="库表结构" key="2">
                <TMetaDataTable projectId={id}/>
            </TabPane>
            <TabPane tab="API接口" key="3">
                <Interface projectId={id}/>
            </TabPane>
            <TabPane tab="项目成员" key="4">
                <ProjectUser projectId={id}/>
            </TabPane>
            <TabPane tab="设置" key="5">
                <ProjectInfo projectId={id}/>
            </TabPane>
        </Tabs>
    </div>
};

export default ProjectInfo;
