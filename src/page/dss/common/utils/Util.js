const colors = ['#1890FF', '#41D9C7', '#2FC25B', '#FACC14', '#9AE65C','#2db7f5','#f50','#87d068','#108ee9','magenta','red','volcano','orange','gold','lime','green','cyan','blue','geekblue','purple'];

const Util={
    getRandomColor:(useColors=colors)=>{
        return useColors[Math.floor(Math.random() * colors.length)];
    }
};

export default Util;
