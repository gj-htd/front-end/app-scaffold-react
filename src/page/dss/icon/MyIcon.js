import { createFromIconfontCN } from '@ant-design/icons';
const MyIcon = createFromIconfontCN({
    scriptUrl:[
        '//at.alicdn.com/t/font_1992939_55uo1zl7rra.js', // 在 iconfont.cn 上生成 icon-defaultProjectIcon
    ]
});

export {MyIcon};
