import {Table, Badge, Menu, Dropdown, Space, Button} from 'antd';
import React from "react";
import { DownOutlined } from '@ant-design/icons';
import {NormalOptionsButton} from "../../../../core";

const menu = (
    <Menu>
        <Menu.Item>Action 1</Menu.Item>
        <Menu.Item>Action 2</Menu.Item>
    </Menu>
);

function TMetaDataTable() {
    const expandedRowRender = () => {
        const columns = [
            { title: '列名称', dataIndex: 'name', key: 'title' },
            { title: 'code', dataIndex: 'code', key: 'target' },
            { title: '数据类型', dataIndex: 'dataType', key: 'dataType'},
            { title: '注释', dataIndex: 'comment', key: 'comment'},
            { title: '默认值', dataIndex: 'defaultValue', key: 'defaultValue'},
            { title: '长度', dataIndex: 'dataLength', key: 'dataLength'},
            { title: '主键', dataIndex: 'isPk', key: 'isPk'},
            { title: '约束', dataIndex: 'bound', key: 'bound'}
        ];

        const data = [];
        data.push({
            key: 0,
            name: '主键',
            code: 'id',
            dataType: 'bigint',
        },{
            key: 2,
            name: '用户名称',
            code: 'user_name',
            dataType: 'varchar',
            dataLength: 32,
        },{
            key: 2,
            name: '用户年龄',
            code: 'user_age',
            dataType: 'int',
            dataLength: 32,
        });
        return <Table columns={columns} dataSource={data} pagination={false} />;
    };

    const columns = [
        { title: '表名称', dataIndex: 'name', key: 'name' },
        { title: 'code', dataIndex: 'code', key: 'code' },
        { title: '注释', dataIndex: 'comment', key: 'comment'},
        { title: '负责人', dataIndex: 'dutyUserName', key: 'dutyUserName' },
        { title: '最后修改时间', dataIndex: 'lastDateTime', key: 'lastDateTime' },
        { title: '操作', key: 'operation', render: () => {

                return <div>
                    <Button type="link">
                        关联任务
                    </Button>
                </div>
            } },
    ];

    const data = [];
    data.push({
        key: 0,
        name: '用户表',
        platform: 'sys_user',
        comment: '系统用户管理',
        dutyUserName: "XX",
        lastDateTime: '2014-12-24 23:12:00',
    });

    let optionsProps={
        add:{}
    }
    return (
        <>
            <NormalOptionsButton {...optionsProps}/>
        <Table
            className="components-table-demo-nested"
            columns={columns}
            expandable={{ expandedRowRender }}
            dataSource={data}
        /></>
    );
}
export default TMetaDataTable;
