import React from "react";

const Page1 = () => {
    console.log("------第一个页面");
    return (<div>第一个页面</div>)
};

const Page2 = () => {
    return (<div>第二个页面</div>)
};

export default {Page1, Page2}
