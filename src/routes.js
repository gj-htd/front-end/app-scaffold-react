import NormalLoginForm from "./core/normal/login/Login";
import {LayoutApp} from "./core/normal/laout";
import {Project,ProjectInfo} from "./page/dss/Project";
import React from "react";
import Page1App from "./page/demo/Page1App";
import {UserOutlined, VideoCameraOutlined} from '@ant-design/icons';
import {UserStory} from "./page/dss/Project/UserStory";
import NormalRegister from "./core/normal/register/NormalRegister";
import {Register} from "./page/dss/System";
import {Task} from "./page/dss/task";
const routes = [{
    path: "/login",
    component: NormalLoginForm
}, {
    path: "/register",
    component: Register
}, {
    path: "/",
    component: LayoutApp,
    routes: [
        {
            title: "项目",
            path: "/project",
            component: Project,
            icon: <UserOutlined/>,
        },{
            title: "项目详情",
            path: "/project-info",
            icon: <UserOutlined/>,
            component:ProjectInfo,
        },{
            title: "用户故事",
            path: "/user-story",
            component: UserStory,
            icon: <UserOutlined/>
        },{
            title: "任务",
            path: "/task",
            component: Task,
            icon: <UserOutlined/>
        }, {
            title: "page2",
            path: "/user-story/page2",
            component: Page1App.Page2,
            icon: <VideoCameraOutlined/>
        }
    ]
}];

export {routes};
