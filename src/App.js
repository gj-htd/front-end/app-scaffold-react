import React from 'react';
import './App.less';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {useRouters} from "./core/normal/laout/useRouters";
import {routes} from "./routes";
import {NormalLoginForm} from "./core";
import NormalRegister from "./core/normal/register/NormalRegister";


function App() {
    let {getContentRoute} = useRouters();
    return <Router>
        <Switch>
            <Route path={"/login"} component={NormalLoginForm}/>
            <Route path={"/register"} component={NormalRegister}/>
            {getContentRoute(routes)}
        </Switch>

    </Router>;
}

export default App;
