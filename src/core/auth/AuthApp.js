/**
 * 授权
 */
import {Redirect, withRouter} from "react-router-dom";
import React, {useState} from "react";

/**
 * 授权组件
 * @returns {{AuthRouter: *, setAuthState: setAuthState}}
 */
const useAuth = () => {
    const [isAuthenticated, setIsAuthenticated] = useState(false);
    /**
     * 统一维护登录状态
     */
    const setAuthState = () => {
        console.log("触发");
        setIsAuthenticated(true);
    };

    /**
     * 授权路由
     *
     */
    const AuthRouter = withRouter(
        ({history, props}) => {
            console.log(history);
            console.log(props);
            return isAuthenticated ? (
                <p>
                    <Redirect
                        to={{
                            pathname: "/",
                        }}
                    />
                </p>
            ) : (
                <div>
                    <Redirect
                        to={{
                            pathname: "/login",
                        }}
                    />
                </div>
            )
        }
    );
    return {AuthRouter, setAuthState};
};

export {useAuth};
