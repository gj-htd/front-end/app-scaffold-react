import {Avatar, Badge, Divider, Layout, Menu, Space} from 'antd';
import React, {useState} from "react";
import './layout.less';
import {useRouters} from './useRouters'
import {MenuFoldOutlined, MenuUnfoldOutlined,UserOutlined,MailOutlined} from '@ant-design/icons';
import BreadcrumbHome from "./breadc-rumb/BreadcRumbApp";
import coreSettingConfig from "../../coreSettingConfig";

const {SubMenu} = Menu;
const {Header, Content, Sider} = Layout;

const LayoutApp = (props) => {
    console.log(props);
    let routes = props.routes;
/*    console.log(routes);
    console.log("===="+routes.routes);*/
    const {getMenuApp, getContentRoute} = useRouters();
    const [collapsed, setCollapsed] = useState(false);
    const toggle = () => {
        setCollapsed(!collapsed);
    };

    return (
        <Layout style={{minHeight: '100vh'}}>
            <Sider trigger={null} collapsible collapsed={collapsed}>
                <div className="logo">
                    <img src={coreSettingConfig.Logo}/>
                </div>
                {getMenuApp(routes)}
            </Sider>
            <Layout className="site-layout">

{/*                <Header className="site-layout-background" style={{padding: 10}}>
                    {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                        className: 'trigger',
                        onClick: toggle,
                    })}
                    <div>
                        <Menu mode="horizontal">
                            <Badge count={0}>
                                <Avatar icon={ <MailOutlined twoToneColor="#eb2f96" />}/>
                            </Badge>
                            <SubMenu title={
                                    <Badge count={1}>
                                        <Avatar icon={<UserOutlined/>}/>
                                    </Badge>
                                    }>
                                <Menu.Item key="logout">
                                    登出
                                </Menu.Item>
                            </SubMenu>
                        </Menu>
                    </div>
                </Header>*/}

                <Content
                    className="site-layout-background"
                    style={{
                        margin: '24px 16px',
                        padding: 24,
                        minHeight: 280,
                    }}
                >
                    <BreadcrumbHome routes={routes}/>
                    <Divider/>
                    <div className="site-layout-background">
                    {
                        getContentRoute(routes)
                    }
                    </div>
                </Content>
            </Layout>
        </Layout>)
};

export default LayoutApp;
