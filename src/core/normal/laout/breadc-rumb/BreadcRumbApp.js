import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import {Breadcrumb} from 'antd';
import {useRouters} from "../useRouters";


/*const breadcrumbNameMap = {
    '/apps': 'Application List',
    '/page3': 'page3',
    '/page2': 'page2',
    '/page1': 'page1',
    '/apps/1': 'Application1',
    '/apps/2': 'Application2',
    '/apps/1/detail': 'Detail',
    '/apps/2/detail': 'Detail',
};*/
const BreadcrumbHome = withRouter(props => {
    console.log(props);
    const {getBreadcrumbNameMap} = useRouters();

    const breadcrumbNameMap = getBreadcrumbNameMap(props.routes);
    console.log(breadcrumbNameMap);

    const {location} = props;
    const pathSnippets = location.pathname.split('/').filter(i => i);
    const extraBreadcrumbItems = pathSnippets.map((_, index) => {
        const url = `/${pathSnippets.slice(0, index + 1).join('/')}`;
        return (
            <Breadcrumb.Item key={url}>
                <Link to={url}>{breadcrumbNameMap[url]}</Link>
            </Breadcrumb.Item>
        );
    });

    const breadcrumbItems = [
        <Breadcrumb.Item key="home">
            <Link to="/">首页</Link>
        </Breadcrumb.Item>,
    ].concat(extraBreadcrumbItems);
    return (
        <div className="demo">
            <Breadcrumb>{breadcrumbItems}</Breadcrumb>
        </div>
    );
});

export default BreadcrumbHome;
