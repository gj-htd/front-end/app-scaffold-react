import {Avatar, Badge, Divider, Layout, Menu, Space} from 'antd';
import React, {useState} from "react";
import './layout.less';
import {useRouters} from './useRouters'
import {MenuFoldOutlined, MenuUnfoldOutlined,UserOutlined,MailOutlined} from '@ant-design/icons';
import BreadcrumbHome from "./breadc-rumb/BreadcRumbApp";
import coreSettingConfig from "../../coreSettingConfig";
import {routes as myRouter} from '../../../routes';
import {BrowserRouter as Router, Link, Route} from "react-router-dom";
import {Switch} from "react-router";
const {SubMenu} = Menu;
const {Header, Content, Sider} = Layout;

const LayoutTest = (props) => {
    console.log(props);
    let routes =[];
    if(props && props.router){
        routes=props.router;
    }else{
        routes=myRouter;
    }

    const {getMenuApp} = useRouters();
    const [collapsed, setCollapsed] = useState(false);
    const toggle = () => {
        setCollapsed(!collapsed);
    };

    return (
        <Layout style={{minHeight: '100vh'}}>
            <Sider trigger={null} collapsible collapsed={collapsed}>
                <div className="logo">
                    <img src={coreSettingConfig.Logo}/>
                </div>
                {
                    <Menu mode="inline"
                          theme="dark"
                          defaultSelectedKeys={['1']}
                    >
                        {
                            routes.map((item,i)=>{
                                return <Menu.Item key={item.path} icon={item.icon}>
                                    <Link
                                        to={item.path} key={item.path}>{item.title}</Link>
                                </Menu.Item>
                            })
                        }

                    </Menu>
                }
            </Sider>
            <Content>
                <Switch>
                    {routes.map((item,i)=>{
                        return <Route {...item}  strict ></Route>
                    })}
                    <Route component={()=><div>哦豁，404喽！</div>}/>
                </Switch>

            </Content>
        </Layout>)
};

export default LayoutTest;
