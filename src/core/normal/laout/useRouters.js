import {Menu} from "antd";
import React from "react";
import {Link, Route} from "react-router-dom";

const {SubMenu} = Menu;

const useRouters = () => {
    const RouteWithSubRoutes = route => {
        return <Route
            path={route.path}
            render={props => (
                // pass the sub-routes down to keep nesting
                <route.component {...props} routes={route.routes}/>
            )}
        />
    };


    let getMenu = (children) => {
        return children.map((item) => {
            console.log(item);
            if (item.show) {
                return getMenuItem(item);
            } else {
                return null;
            }
        })
    };

    let getMenuItem = (item) => {
        return (
            <Menu.Item key={item.path} icon={item.icon}>
                <Link
                    to={item.path} key={item.path}>{item.title}</Link>
            </Menu.Item>
        )
    };
    const getMenuApp = (routes) => {
        return (<Menu mode="inline"
                      theme="dark"
                      defaultSelectedKeys={['1']}
        >
            {
                routes.map((item, index) => {
                    let isSubMenu = false;
                    let children = item.children;
                    if (children && children.length > 0) {
                        return (
                            <SubMenu key={item.title} icon={item.icon} title={item.title}>
                                {item.children && item.children.length > 0 ? getMenu(item.children) : null}
                            </SubMenu>
                        )
                    } else if(!item.hidden){
                        return getMenuItem(item);
                    }
                })
            }
        </Menu>)
    };


    /**
     * 外部路由
     * @returns {*[]}
     */
    const getRoute = (routes) => {
        return routes.map((route, i) => <RouteWithSubRoutes key={i} {...route} />);
    };

    const breadcrumbNameMap = {};

/*    let getRouteApp=(route)=>{
        return routes.map((item)=>{
            return getRouteApp(item);
        })
    }*/
    /**
     * 获取二级内容路由
     * @param routes
     * @returns {*}
     */
    const getContentRoute = (routes) => {
        return routes.map((item,i)=>{
            return <RouteWithSubRoutes key={i} {...item} />
        });
    };
    /***
     * 面包屑数据组件
     * @param routes
     * @returns {{}}
     */
    const getBreadcrumbNameMap = (routes) => {
        if (routes && routes.length > 0) {
            routes.forEach((route) => {
                breadcrumbNameMap[route.path] = route.title;
            });
        }
        return breadcrumbNameMap;
    };
    return {getMenuApp, getRoute, getContentRoute, getBreadcrumbNameMap};
};

export {useRouters}

