import request from "../../core/request";
class BaseApi {
    constructor(iPath){
        this.path = iPath;
    }
      path="/";
    request=request;
     listPage=(params)=>{
         console.log("参数:")
         console.log(params)
        return request.get
        (this.path, {params:params});
    };
     add=(params)=>{
        return request.post(this.path,{data:params})
    };
     update=(params)=>{
         return request.post(this.path+"/"+params.id,{data:params})
     }
     save=(params)=>{
        if(params.id){
            return this.update(params);
        }else{
            return this.add(params);
        }
     }
     detail=(id)=>{
        return request.get(this.path+"/"+id);
    };
     del=(ids)=>{
        return request.delete(this.path+"/"+ids);
    };
}


export default BaseApi;


