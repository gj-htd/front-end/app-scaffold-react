import {Button, Card, Checkbox, Divider, Form, Input} from 'antd';
import {LockOutlined, UserOutlined} from '@ant-design/icons';
import React from "react";
import './login.less';

import {useHistory} from "react-router";
import coreSettingConfig from "../../coreSettingConfig";
const {Meta} = Card;


const NormalLoginForm = (props, ccc) => {
    let history = useHistory();
    console.log(props);
    const onFinish = values => {
        history.push("/");
        console.log('Received values of form: ', values);
    };

    return (
        <header className="App-header">

            <div >
                <div align="center" className="login-img">
                    <img alt={'logo'} src={coreSettingConfig.Logo} width={"70%"}  />
                </div>

                <Card>
                    <Form
                        name="normal_login"
                        className="login-form"
                        initialValues={{
                            remember: true,
                        }}
                        onFinish={onFinish}
                    >
                        <Form.Item
                            name="账号"
                            rules={[
                                {
                                    required: true,
                                    message: '请输入您的账号!',
                                },
                            ]}
                        >
                            <Input prefix={<UserOutlined className="site-form-item-icon"/>} placeholder="账号"/>
                        </Form.Item>
                        <Form.Item
                            name="密码"
                            rules={[
                                {
                                    required: true,
                                    message: '请输入您的密码!',
                                },
                            ]}
                        >
                            <Input
                                prefix={<LockOutlined className="site-form-item-icon"/>}
                                type="password"
                                placeholder="密码"
                            />
                        </Form.Item>
                        <Form.Item>
                            <Form.Item name="remember" valuePropName="checked" noStyle>
                                <Checkbox>记住我</Checkbox>
                            </Form.Item>

                            <a className="login-form-forgot" href="">
                                忘记密码
                            </a>
                        </Form.Item>

                        <Form.Item>
                            <Button type="primary" htmlType="submit" className="login-form-button">
                                登录
                            </Button>
                            或者 <a href="javascript:void(0)" onClick={()=>{
                                history.push("/register");
                        }}>注册!</a>
                        </Form.Item>
                    </Form>
                </Card>

            </div>
        </header>

    );
};
export default NormalLoginForm;
