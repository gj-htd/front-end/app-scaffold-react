import {Button, Popconfirm} from "antd";
import React from "react";
import NormalButtonLayout from "./NormalButtonLayout";
import { EditOutlined, EllipsisOutlined, SettingOutlined,PlusOutlined,DeleteOutlined,QuestionCircleOutlined } from '@ant-design/icons';

/**
 * 方法执行
 * @param option
 */
let executeFn=(option)=>{
    if(option.fn && typeof (option.fn)==="function"){
        if(option.params){
            option.fn(option.params);
        }else{
            option.fn();
        }
    }
}
/**
 * 基础操作按钮
 * @param add
 * @param del
 * @param open
 * @param refresh
 * @returns {*}
 * @constructor
 */
const NormalOptionsButton=({add,del,open,refresh})=>{
    return <NormalButtonLayout>
        {add?<Button  onClick={()=>{
            executeFn(add);
        }}>
            {add.title?add.title:"新增"}
        </Button>:null}
        {del? <Popconfirm title="确认删除？" icon={<QuestionCircleOutlined style={{ color: 'red' }} onClick={()=>{
            executeFn(del);
        }}/>}>
            <Button title={"删除"} danger>{del.title?del.title:"删除"}</Button>
        </Popconfirm>:null}
    </NormalButtonLayout>

};

export default NormalOptionsButton;
