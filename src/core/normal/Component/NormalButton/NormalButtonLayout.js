import {Col, Row, Space} from "antd";
import React from "react";

const NormalButtonLayout=({children})=>{
    return <Row justify="end">
        <Space style={{ marginBottom: 16 }} size={"small"}>
            {children && children.length>1 ?children.map(function (item) {
                return <Col>{item}</Col>
            }): <Col>{children}</Col>}

        </Space>
    </Row>
};
export default NormalButtonLayout;
